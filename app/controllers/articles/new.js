import Ember from 'ember';

export default Ember.Controller.extend({
isValid: Ember.computed(
            'model.description',
            function() {
                return !Ember.isEmpty(this.get('model.description'));
            }
        ),
        actions: {
            save() {
                if (this.get('isValid')) {
                    this.get('model').save().then(() => {
                        this.transitionTo('articles');
                    });
                } else {
                    this.set('errorMessage', 'You have to fill all the fields');
                }
                },
                cancel() {
                    this.transitionTo('articles');
                }
        }
    });