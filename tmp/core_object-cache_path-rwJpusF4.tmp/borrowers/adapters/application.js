define('borrowers/adapters/application', ['exports', 'active-model-adapter'], function (exports, ActiveModelAdapter) {

  'use strict';

  //
  // Import the default export from active-model-adapter
  //
  exports['default'] = ActiveModelAdapter['default'].extend({
    namespace: 'api'
  });

});