define('borrowers/routes/friends/index', ['exports', 'ember'], function (exports, Ember) {

	'use strict';

	exports['default'] = Ember['default'].Route.extend({
		//
		// Here we are using ES6 syntax for functions!
		// We can use this out of the box with ember-cli
		// thanks to the addon ember-cli-babel
		//
		// To learn more about ES6, check http://s.abuiles.com/bwWo
		//
		model: function model() {
			return this.store.findAll('friend');
		},

		actions: {
			'delete': function _delete(friend) {
				friend.destroyRecord();
				return false;
			}
		}
	});

});