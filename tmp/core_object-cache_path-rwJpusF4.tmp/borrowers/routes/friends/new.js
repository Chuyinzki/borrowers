define('borrowers/routes/friends/new', ['exports', 'ember'], function (exports, Ember) {

	'use strict';

	exports['default'] = Ember['default'].Route.extend({
		model: function model() {
			return this.store.createRecord('friend');
		},
		actions: {
			save: function save() {
				console.log('+-- save action bubbled up to friends new route');
				return false;
			},
			cancel: function cancel() {
				console.log('+-- cancel action bubbled up to friends new route');
				return false;
			}
		}
	});

});