define('borrowers/routes/application', ['exports', 'ember'], function (exports, Ember) {

	'use strict';

	exports['default'] = Ember['default'].Route.extend({
		actions: {
			save: function save() {
				console.log('+---- save action bubbled up to application route');
				return true;
			},
			cancel: function cancel() {
				console.log('+---- cancel action bubbled up to application route');
				return true;
			}
		}
	});

});