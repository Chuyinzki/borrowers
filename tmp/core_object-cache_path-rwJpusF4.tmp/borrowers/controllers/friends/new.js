define('borrowers/controllers/friends/new', ['exports', 'borrowers/controllers/friends/base'], function (exports, FriendsBaseController) {

	'use strict';

	exports['default'] = FriendsBaseController['default'].extend({
		actions: {
			cancel: function cancel() {
				this.get('model').destroyRecord();
				this.transitionToRoute('friends');
				return false;
			}
		}
	});

});