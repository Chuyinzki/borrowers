define('borrowers/controllers/friends/edit', ['exports', 'borrowers/controllers/friends/base'], function (exports, FriendsBaseController) {

  'use strict';

  exports['default'] = FriendsBaseController['default'].extend({
    actions: {
      cancel: function cancel() {
        this.transitionToRoute('friends.show', this.get('model'));
        return false;
      }
    }
  });

});