define('borrowers/controllers/friends/base', ['exports', 'ember'], function (exports, Ember) {

	'use strict';

	exports['default'] = Ember['default'].Controller.extend({
		isValid: Ember['default'].computed('model.email', 'model.firstName', 'model.lastName', 'model.twitter', function () {
			return !Ember['default'].isEmpty(this.get('model.email')) && !Ember['default'].isEmpty(this.get('model.firstName')) && !Ember['default'].isEmpty(this.get('model.lastName')) && !Ember['default'].isEmpty(this.get('model.twitter'));
		}),
		actions: {
			save: function save() {
				if (this.get('isValid')) {
					var _this = this;
					this.get('model').save().then(function (friend) {
						_this.transitionToRoute('friends.show', friend);
					});
				} else {
					this.set('errorMessage', 'You have to fill all the fields');
				}
				return false;
			},
			cancel: function cancel() {
				return true;
			}
		}
	});

});