define('borrowers/models/friend', ['exports', 'ember-data'], function (exports, DS) {

  'use strict';

  exports['default'] = DS['default'].Model.extend({
    firstName: DS['default'].attr('string'),
    lastName: DS['default'].attr('string'),
    email: DS['default'].attr('string'),
    twitter: DS['default'].attr('string'),
    totalArticles: DS['default'].attr('number'),
    fullName: Ember.computed('firstName', 'lastName', {
      get: function get() {
        return this.get('firstName') + ' ' + this.get('lastName');
      }
    })
  });

});