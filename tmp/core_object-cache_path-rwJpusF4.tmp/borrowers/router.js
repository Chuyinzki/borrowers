define('borrowers/router', ['exports', 'ember', 'borrowers/config/environment'], function (exports, Ember, config) {

  'use strict';

  var Router = Ember['default'].Router.extend({
    location: config['default'].locationType
  });

  Router.map(function () {
    this.route('friends', function () {
      this.route('new', {});

      this.route('show', {
        path: ':friend_id'
      });

      this.route('edit', {
        path: ':friend_id/edit'
      });
    });
  });

  exports['default'] = Router;

});