export default Ember.HTMLBars.template((function() {
  return {
    meta: {
      "revision": "Ember@1.13.7",
      "loc": {
        "source": null,
        "start": {
          "line": 1,
          "column": 0
        },
        "end": {
          "line": 13,
          "column": 7
        }
      },
      "moduleName": "borrowers/templates/friends/-form.hbs"
    },
    arity: 0,
    cachedFragment: null,
    hasRendered: false,
    buildFragment: function buildFragment(dom) {
      var el0 = dom.createDocumentFragment();
      var el1 = dom.createElement("form");
      var el2 = dom.createTextNode("\n");
      dom.appendChild(el1, el2);
      var el2 = dom.createElement("h2");
      var el3 = dom.createComment("");
      dom.appendChild(el2, el3);
      dom.appendChild(el1, el2);
      var el2 = dom.createTextNode("\n");
      dom.appendChild(el1, el2);
      var el2 = dom.createElement("fieldset");
      var el3 = dom.createTextNode("\n");
      dom.appendChild(el2, el3);
      var el3 = dom.createComment("");
      dom.appendChild(el2, el3);
      var el3 = dom.createElement("br");
      dom.appendChild(el2, el3);
      var el3 = dom.createTextNode("\n");
      dom.appendChild(el2, el3);
      var el3 = dom.createComment("");
      dom.appendChild(el2, el3);
      var el3 = dom.createElement("br");
      dom.appendChild(el2, el3);
      var el3 = dom.createTextNode("\n");
      dom.appendChild(el2, el3);
      var el3 = dom.createComment("");
      dom.appendChild(el2, el3);
      var el3 = dom.createElement("br");
      dom.appendChild(el2, el3);
      var el3 = dom.createTextNode("\n");
      dom.appendChild(el2, el3);
      var el3 = dom.createComment("");
      dom.appendChild(el2, el3);
      var el3 = dom.createElement("br");
      dom.appendChild(el2, el3);
      var el3 = dom.createTextNode("\n");
      dom.appendChild(el2, el3);
      var el3 = dom.createElement("input");
      dom.setAttribute(el3,"type","submit");
      dom.setAttribute(el3,"value","Save");
      dom.setAttribute(el3,"class","primary");
      dom.appendChild(el2, el3);
      var el3 = dom.createTextNode("\n");
      dom.appendChild(el2, el3);
      var el3 = dom.createElement("button");
      var el4 = dom.createTextNode("Cancel");
      dom.appendChild(el3, el4);
      dom.appendChild(el2, el3);
      var el3 = dom.createTextNode("\n");
      dom.appendChild(el2, el3);
      dom.appendChild(el1, el2);
      var el2 = dom.createTextNode("\n");
      dom.appendChild(el1, el2);
      dom.appendChild(el0, el1);
      return el0;
    },
    buildRenderNodes: function buildRenderNodes(dom, fragment, contextualElement) {
      var element0 = dom.childAt(fragment, [0]);
      var element1 = dom.childAt(element0, [3]);
      var element2 = dom.childAt(element1, [15]);
      var morphs = new Array(7);
      morphs[0] = dom.createElementMorph(element0);
      morphs[1] = dom.createMorphAt(dom.childAt(element0, [1]),0,0);
      morphs[2] = dom.createMorphAt(element1,1,1);
      morphs[3] = dom.createMorphAt(element1,4,4);
      morphs[4] = dom.createMorphAt(element1,7,7);
      morphs[5] = dom.createMorphAt(element1,10,10);
      morphs[6] = dom.createElementMorph(element2);
      return morphs;
    },
    statements: [
      ["element","action",["save"],["on","submit"],["loc",[null,[1,6],[1,35]]]],
      ["content","errorMessage",["loc",[null,[2,4],[2,20]]]],
      ["inline","input",[],["value",["subexpr","@mut",[["get","model.firstName",["loc",[null,[4,14],[4,29]]]]],[],[]],"placeholder","First Name"],["loc",[null,[4,0],[4,56]]]],
      ["inline","input",[],["value",["subexpr","@mut",[["get","model.lastName",["loc",[null,[5,14],[5,28]]]]],[],[]],"placeholder","Last Name"],["loc",[null,[5,0],[5,54]]]],
      ["inline","input",[],["value",["subexpr","@mut",[["get","model.email",["loc",[null,[6,14],[6,25]]]]],[],[]],"placeholder","email"],["loc",[null,[6,0],[7,21]]]],
      ["inline","input",[],["value",["subexpr","@mut",[["get","model.twitter",["loc",[null,[8,14],[8,27]]]]],[],[]],"placeholder","twitter"],["loc",[null,[8,0],[9,23]]]],
      ["element","action",["cancel"],[],["loc",[null,[11,8],[11,27]]]]
    ],
    locals: [],
    templates: []
  };
}()));