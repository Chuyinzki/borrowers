import Ember from 'ember';

export default function () {
  Ember.Test.registerAsyncHelper('select', function (app, selector) {
    for (var _len = arguments.length, texts = Array(_len > 2 ? _len - 2 : 0), _key = 2; _key < _len; _key++) {
      texts[_key - 2] = arguments[_key];
    }

    var $options = app.testHelpers.findWithAssert(selector + ' option');

    $options.each(function () {
      var _this = this;

      var $option = Ember.$(this);

      Ember.run(function () {
        _this.selected = texts.some(function (text) {
          return $option.is(':contains(\'' + text + '\')');
        });
        $option.trigger('change');
      });
    });

    return app.testHelpers.wait();
  });
}