import Ember from 'ember';

export default Ember.Controller.extend({
    isValid: Ember.computed('model.description', function () {
        return !Ember.isEmpty(this.get('model.description'));
    }),
    actions: {
        save: function save() {
            var _this = this;

            if (this.get('isValid')) {
                this.get('model').save().then(function () {
                    _this.transitionTo('articles');
                });
            } else {
                this.set('errorMessage', 'You have to fill all the fields');
            }
        },
        cancel: function cancel() {
            this.transitionTo('articles');
        }
    }
});