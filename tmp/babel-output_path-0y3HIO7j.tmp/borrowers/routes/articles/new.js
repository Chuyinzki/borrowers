import Ember from 'ember';
export default Ember.Route.extend({
    model: function model() {
        return this.store.createRecord('article', {
            friend: this.modelFor('friends/show')
        });
    }
});