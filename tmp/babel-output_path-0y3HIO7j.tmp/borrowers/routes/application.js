import Ember from 'ember';

export default Ember.Route.extend({
	actions: {
		save: function save() {
			console.log('+---- save action bubbled up to application route');
			return true;
		},
		cancel: function cancel() {
			console.log('+---- cancel action bubbled up to application route');
			return true;
		}
	}
});