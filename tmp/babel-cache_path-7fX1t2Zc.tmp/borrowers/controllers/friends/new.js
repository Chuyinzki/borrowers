import FriendsBaseController from './base';

export default FriendsBaseController.extend({
	actions: {
		cancel: function cancel() {
			this.get('model').destroyRecord();
			this.transitionToRoute('friends');
			return false;
		}
	}
});