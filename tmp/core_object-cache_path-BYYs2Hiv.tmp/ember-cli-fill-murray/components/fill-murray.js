define('ember-cli-fill-murray/components/fill-murray', ['exports', 'ember', 'ember-cli-fill-murray/templates/components/fill-murray'], function (exports, Ember, layout) {

    'use strict';

    exports['default'] = Ember['default'].Component.extend({
        layout: layout['default'],
        height: 100, // Default height and width 100
        width: 100,
        //
        // The following computed property will give us the url for
        // fill-murray. In this case it depends on the properties height and width.
        //
        src: Ember['default'].computed('height', 'width', {
            get: function get() {
                var base = 'http://www.fillmurray.com/';
                return '' + base + this.get('width') + '/' + this.get('height');
            }
        })
    });

});